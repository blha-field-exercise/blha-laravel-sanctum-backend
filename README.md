# blha-laravel-sanctum-backend

By
[Siegfried Schweizer](https://gitlab.com/SiegfriedSchweizer) ([website](https://sigi-schweizer.de/))

This is the backend part of a field exercise given to me by [BLHA](https://blha.brandenburg.de/). It may also serve as an 
example for a simple web application where frontend and backend are completely decoupled, that is, the frontend communicates 
with the backend via REST API only.

In order to emphasize the latter, the field exercise consists of two separate [Laravel](https://laravel.com/) projects: 
one for the frontend and one for the backend.

This one, the backend part, just contains the database used by the web application and the REST API to read from and write 
to the database. The only extra dependency it has got is [Laravel Sanctum](https://laravel.com/docs/10.x/sanctum) which 
“provides a featherweight authentication system for SPAs (single page applications), mobile applications, and simple, token 
based APIs”.

Developed and tested under [Debian 11](https://www.debian.org/) with PHP v8.2.13, Composer v2.2.7, and Laravel v10.34.2. 
You will likely need to install [Composer](https://getcomposer.org/download/) if you don't have it in order to run the project.

## Installation

You may clone this repository anywhere you want on your computer since it is intended to run on Laravel's built-in development 
server.

```sh
git clone https://gitlab.com/blha-field-exercise/blha-laravel-sanctum-backend.git
cd blha-laravel-sanctum-backend
composer install
php artisan migrate
```

If being asked if you would you like to create the SQLite database, please choose “Yes”.

Other than proposed in the field exercise specification I rather added a Seeder instead of a SQL dump in order to add sample 
data to the application:

```sh
php artisan db:seed --class=UserSeeder
```

Now you will be able to run the thing:

```sh
php artisan serve
```

And watch it in your Browser: http://127.0.0.1:8000/ ... where you won't see much yet because it only makes sense in conjunction 
with [blha-laravel-bootstrap-frontend](https://gitlab.com/blha-field-exercise/blha-laravel-bootstrap-frontend), which you should clone and install now as well.
