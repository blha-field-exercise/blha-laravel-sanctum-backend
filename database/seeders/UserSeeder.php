<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            'name' => 'Siegfried',
            'email' => 'siegfried@localhost',
            'password' => Hash::make('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Coração',
            'email' => 'coracao@localhost',
            'password' => Hash::make('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Señora Tentación',
            'email' => 'tentacion@localhost',
            'password' => Hash::make('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Mimí',
            'email' => 'mimi@localhost',
            'password' => Hash::make('secret'),
        ]);
        DB::table('users')->insert([
            'name' => 'Ariel',
            'email' => 'ariel@localhost',
            'password' => Hash::make('secret'),
        ]);
    }
}
